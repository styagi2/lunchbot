import json
import random
# import rhinoscriptsyntax as rs
import requests


class Json_handler(object):
    def __init__(self):
        self.Restaurants = None

    def open_json(self, filename):
        with open(filename, 'r') as f:
            self.Restaurants = json.load(f)

    def make_api_request(self, params=None):
        """ returns a random Restaurant object from the search results """
        param_dict = {"term": "food", "latitude" : 42.282275, "longitude": -83.749887, "radius": 1600, "limit": 40}
        if params is not None:
            for key, value in params.iteritems():
                param_dict[key] = value
        response=requests.get('https://api.yelp.com/v3/businesses/search', param_dict, headers={"Authorization" : "Bearer B0BibwYbI77i0ZU8x1AyyfGuMhoDFW-xylpfGvTUhUCfHpQzVxsMERdc9SBUsnUjXAQGQnNrSOyQcd2mCh698256Tg7zbg2LIdIn31l34LNnyXBXsAylGdoLr5oTXXYx"})
        rand_rest = self.get_random_restaurant(response.json())
        rand_rest_dict = {"name": rand_rest.name, "rating": rand_rest.rating, "price": rand_rest.price, "walking_time": rand_rest.walking_time}
        return rand_rest_dict

    def get_random_restaurant(self, converted_json_file):
        #import pdb
        #pdb.set_trace()

        num_restaurants = converted_json_file["total"]
        restaurant_index = random.randint(0, min(40, num_restaurants))
        chosen_restaurant = converted_json_file["businesses"][restaurant_index] 
        
        name = chosen_restaurant["name"]
        rating = chosen_restaurant["rating"]
        price = chosen_restaurant["price"]
        is_closed = chosen_restaurant["is_closed"]
        # categories is a list of dictionaries (alias: "", title: "")
        categories = chosen_restaurant["categories"]
        # distance is in meters
        distance = chosen_restaurant["distance"]

        return Restaurant(name, rating, price, not is_closed, categories, distance)


class Restaurant(object):
    def __init__(self,
        name = None,
        rating = None,
        price = None,
        is_open = None,
        categories = None,
        distance = None
    ):
        if name is not None:
            self.name = name
        if rating is not None:
            self.rating = rating
        if price is not None:
            self.price = price
        if is_open is not None:
            self.is_open = is_open
        if categories is not None:
            self.categories = categories
        if distance is not None:
            self.distance = distance
            self.walking_time = int(distance / 84)
    
if __name__ == "__main__":
    j = Json_handler()
    rest = j.make_api_request() 
    print rest

