from flask import Flask, request, jsonify
import json
from urlparse import parse_qs, urlparse
import lunchbot
from sendpost import send_post
from parse_json import Json_handler


app = Flask(__name__)
@app.route('/slack/lunchbot', methods=['POST', 'GET'])
def result():
    payload = '/?' + request.get_data()
    values = parse_qs(urlparse(payload).query, keep_blank_values=True)
    text = values["text"]
    channel_id = values["channel_id"]
    restaurant = lunchbot.handle_command(channel_id[0], text[0])
    if type(restaurant) == str:
        return jsonify({"text": restaurant})
    # return jsonify({"channel":channel_id, "text":"Bon appetit!", "attachments":[{"text":restaurant["name"]}]})

    return jsonify({
        "text": "{}\nRating: {}\nPrice: {}\n{} minutes away\n".format(restaurant["name"], restaurant["rating"], restaurant["price"], restaurant["walking_time"])
    })
if __name__ == "__main__":
    app.run()