import requests

# destination = "https://hooks.slack.com/services/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"
destination = "https://slack.com/api/chat.postMessage"

# Function that takes in a json formatted dictionary and sends it to the destination server above
def send_post(channel_id, restaurant_name):
	print(channel_id, "    ", restaurant_name)
	yelp_results = {"channel":channel_id, "text":"This is a lunchbot test!", "attachments":[{"text":restaurant_name}]}
	return yelp_results
