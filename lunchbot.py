from sendpost import send_post
from parse_json import Json_handler

def handle_command(channel_id, request):
    """
        Executes bot command if the command is known.
    """

    get_response = Json_handler()
    response = None

    # This is where you start to implement more commands!
    if request == '' or request == 'help':
        response = user_help()
    elif request == 'random':
        response = get_response.make_api_request()
    elif request.startswith('$'):
        if request == '$':
            response = get_response.make_api_request({'price': '1'})
        elif request == '$$':
            response = get_response.make_api_request({'price': '1, 2'})
        elif request == '$$$':
            response = get_response.make_api_request({'price': '1, 2, 3'})
        else:
            response = get_response.make_api_request({'price': '1, 2, 3, 4'})
    elif request.startswith('category'):
        category = request.split('=')[1]
        response = get_response.make_api_request({'categories': category})
    else:
        response = user_help()

    return response


def user_help():
    return 'Run /lunchbot followed by one of these options: [random, $, $$, $$$, $$$$, category=<Cuisine>]'